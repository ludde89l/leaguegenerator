let $pills = $('a[data-toggle="pill"]');

$pills.on('shown.bs.tab', function () {
    let tab = $(this).parent();
    let first = tab.parent().hasClass('first');
    let last = tab.parent().hasClass('last');
    disableTabs(tab);
    $("#previous").prop("disabled",first);
    if(last) {
        $("#next").parent().addClass("d-none");
        $("#finish").parent().removeClass("d-none");
    }
    else {
        $("#next").parent().removeClass("d-none");
        $("#finish").parent().addClass("d-none");
    }
});

function hideTab($tab) {
    $tab.addClass("d-none");
    let parent = $tab.parent();
    let tab = parent.find(".active").parent();
    disableTabs(tab);
}

function showTab($tab) {
    $tab.removeClass("d-none");
    let parent = $tab.parent();
    let tab = parent.find(".active").parent();
    disableTabs(tab);
}

function disableTabs($activeTab) {
    $activeTab.nextAll(':not(.d-none)').each(function () {
        $(this).children().first().addClass('disabled');
    });
    $activeTab.prevAll(':not(.d-none)').each(function () {
        $(this).children().first().removeClass('disabled');
    });
    let next = $activeTab.nextAll('li:not(.d-none)').first();
    next.children().first().removeClass('disabled');

}

function previousClicked() {
    let $tabs = $('#pills-tab');
    let $parent = $tabs.find(".active").parent();
    let $next = $parent.prevAll();
    let $nextTabs = $next.filter(function () {
        return !$(this).hasClass('d-none');
    });
    if($nextTabs) {
        $nextTabs.first().children().first().tab('show')
    }
}

function nextClicked() {
    let $tabs = $('#pills-tab');
    let $parent = $tabs.find(".active").parent();
    let $next = $parent.nextAll();
    let $nextTabs = $next.filter(function () {
        return !$(this).hasClass('d-none');
    });
    if($nextTabs) {
        $nextTabs.first().children().first().tab('show')
    }
}

$(".next").click(function(){ nextClicked(); });
$(".previous").click(function(){ previousClicked(); });
