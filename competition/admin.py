from django.contrib import admin
from competition.models import *


class NewsDataInline(admin.StackedInline):
    model = NewsData
    extra = 3


class NewsAdmin(admin.ModelAdmin):
    inlines = [NewsDataInline]


admin.site.register(Tournament)
admin.site.register(TieBreakerOrder)
admin.site.register(Round)
admin.site.register(Player)
admin.site.register(Match)
admin.site.register(Result)
admin.site.register(Stage)
admin.site.register(Playoff)
admin.site.register(PlayoffRound)
admin.site.register(Rule)
admin.site.register(MatchOrder)
admin.site.register(MatchData)
admin.site.register(News, NewsAdmin)
