from django.db import models
from model_utils.managers import InheritanceManager
from competition import util
from django.contrib.auth.models import User


class TieBreaker(models.Model):
    objects = InheritanceManager()
    name = models.CharField(max_length=200, db_index=True, unique=True)
    type = models.CharField(max_length=5, db_index=True, unique=True)

    def __str__(self):
        return self.name


class Player(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)
    display_name = models.CharField(max_length=200, unique=True)
    friends = models.ManyToManyField("self")

    def get_display_name(self):
        return self.display_name or self.user.username

    def __str__(self):
        return self.get_display_name()

class Tournament(models.Model):
    FOOTBALL = 'FOOT'
    TOURNAMENT_TYPES = (
        (FOOTBALL, 'Football'),
    )
    name = models.CharField(max_length=200, unique=True)
    tie_breakers = models.ManyToManyField(TieBreaker, through='TieBreakerOrder')
    type = models.CharField(max_length=4, choices=TOURNAMENT_TYPES)
    ext_id = models.CharField(max_length=10, unique=True)
    winners = models.ManyToManyField(Player)
    protected = models.BooleanField(default=False)
    unlisted = models.BooleanField(default=False)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.ext_id:
            self.ext_id = util.generate_id(10)
        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    def __str__(self):
        return '{0}({1})'.format(self.name, self.ext_id)

    @staticmethod
    def visible():
        return Tournament.objects.filter(unlisted=False)


class TournamentPermissions(models.Model):

    READ = 'R'
    READ_WRITE = 'R/W'
    PERMISSION_TYPES = (
        (READ, 'Read'),
        (READ_WRITE, 'Read/Write'),
    )
    access = models.CharField(max_length=4, choices=PERMISSION_TYPES)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("tournament", "user")


class TieBreakerOrder(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    tie_breaker = models.ForeignKey(TieBreaker, on_delete=models.CASCADE)
    order = models.IntegerField()

    class Meta:
        ordering = ['order']
        unique_together = (("tournament", "order"), ("tie_breaker", "tournament"),)


class Rule(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    key = models.CharField(max_length=40)
    value = models.CharField(max_length=100)


    def __str__(self):
        return str(self.tournament) + ': ' + str(self.key) + ' = ' + str(self.value)

    class Meta:
        unique_together = ("tournament", "key")


class Match(models.Model):
    competitors = models.ManyToManyField(Player, through='MatchData')
    type = models.CharField(max_length=4, choices=Tournament.TOURNAMENT_TYPES)

    def __str__(self):
        return ' - '.join(x.get_display_name() for x in self.competitors.all())


MATCH_PLACEMENT = (
    ('H', 'Home'),
    ('A', 'Away'),
    ('N', 'Neutral'),
)


class MatchData(models.Model):
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    placement = models.CharField(max_length=1, choices=MATCH_PLACEMENT)


class Stage(models.Model):
    type = models.CharField(max_length=20, db_index=True, unique=True)
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.type

    class Meta:
        ordering = ['order']


class Round(models.Model):
    """Represents group stage, qtr final, semi final and so on"""
    name = models.CharField(max_length=200)
    competitors = models.ManyToManyField(Player, blank=True)
    matches = models.ManyToManyField(Match, blank=True, through='MatchOrder')
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE)

    def __str__(self):
        return '{0}: {1}'.format(str(self.tournament), self.name)

    class Meta:
        ordering = ['name']
        unique_together = ('name', 'tournament')


class MatchOrder(models.Model):
    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    match = models.OneToOneField(Match, on_delete=models.CASCADE)
    order = models.IntegerField()

    def __str__(self):
        return '{0}, {1}'.format(str(self.round), str(self.match))

    class Meta:
        ordering = ['order']
        unique_together = (('round', 'order'), ('match',))


class Playoff(models.Model):
    objects = InheritanceManager()

    tournament = models.OneToOneField(Tournament, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.tournament)


class PlayoffRound(Round):
    """Used to point to the next round for the teams in this round, should be blank for the final"""
    next = models.ForeignKey('PlayoffRound', blank=True, null=True, on_delete=models.CASCADE)
    playoff = models.ForeignKey(Playoff, on_delete=models.CASCADE)


class Result(models.Model):
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    score = models.PositiveIntegerField()

    def __str__(self):
        return str(self.player) + " " + str(self.score)

    class Meta:
        unique_together = (("match", "player"),)


class News(models.Model):
    publish_date = models.DateTimeField()
    headline = models.TextField()
    text = models.TextField()
    link = models.TextField(blank=True)
    # News about a special player, if blank the news is general
    owner = models.ForeignKey(Player, blank=True, null=True, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-publish_date']


class NewsData(models.Model):
    key = models.CharField(max_length=20)
    value = models.TextField()
    news = models.ForeignKey(News, on_delete=models.CASCADE)
