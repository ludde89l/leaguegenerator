from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.forms.fields import EmailField
from django.contrib.auth.models import User
from django.db import transaction
from django.utils.translation import gettext as _

from competition.models import Player


class MyUserCreationForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(MyUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True

    def _post_clean(self):
        super()._post_clean()
        try:
            Player.objects.get(display_name=self.cleaned_data['username'])
        except Player.DoesNotExist:
            return
        self.add_error('username', _('Taken username'))

    @transaction.atomic
    def save(self, commit=True):
        instance = super(MyUserCreationForm, self).save(commit=False)
        instance.player = Player(display_name=instance.username)
        if commit:
            instance.save()
            instance.player.save()
        return instance

    class Meta:
        model = User
        fields = ("username", "email")
        field_classes = {'username': UsernameField, 'email':EmailField}