class LoginNeeded(Exception):
    def __init__(self, login_url):
        self.login_url = login_url


class BadRequest(Exception):
    def __init__(self, message):
        self.message = message