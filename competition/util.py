import string
import random

from django.utils.translation import gettext as _


def generate_id(size, chars=string.ascii_letters + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class TranslatedNews:
    def __init__(self, news):
        self.news = news
        self.values = {data.key:data.value for data in news.newsdata_set.all()}

    def headline(self):
        return _(self.news.headline) % self.values

    def text(self):
        return _(self.news.text) % self.values