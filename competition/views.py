from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.postgres.search import SearchVector
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.shortcuts import render, reverse
from django.views.generic import ListView, FormView

from competition.forms import MyUserCreationForm
from competition.models import Tournament, Player, News
from competition.util import TranslatedNews


def home(request):
    news = News.objects.all()[:3]
    context = {
        'news_list': [TranslatedNews(n) for n in news]
    }
    return render(request, 'competition/home.html', context=context)

@login_required
def my_leagues(request):
    try:
        player = Player.objects.get(user=request.user)
        tournaments = Tournament.objects.filter(round__competitors=player).distinct()
    except Player.DoesNotExist:
        tournaments = []
    context = {
        'object_list': sorted(tournaments, key= lambda x: x.name.lower())
    }
    return render(request, 'competition/tournament_list.html', context=context)


class RegisterView(FormView):

    def get_form_class(self):
        return MyUserCreationForm

    def get_template_names(self):
        return ['competition/register.html']

    def form_valid(self, form):
        username = form.cleaned_data['username']
        form.save()
        user = User.objects.get(username=username)
        login(self.request, user)
        return HttpResponseRedirect(reverse('competition:home'))


class TournamentSearchListView(ListView):
    model = Tournament
    paginate_by = 10

    def get_queryset(self):

        keywords = self.request.GET.get('q')
        tournaments = Tournament.visible()
        if self.request.user.is_authenticated:
            user = self.request.user
            player = Player.objects.get(user=user)
            tournaments = tournaments.filter(Q(protected=False) | Q(round__competitors=player))
        else:
            tournaments = tournaments.filter(protected=False)

        if keywords:
            return tournaments.annotate(search=SearchVector('ext_id', 'name')).filter(search=keywords).order_by('type', 'name').distinct()

        return Tournament.objects.none()
