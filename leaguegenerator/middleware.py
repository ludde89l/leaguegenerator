from competition.exceptions import LoginNeeded, BadRequest
from django.http import HttpResponseRedirect, HttpResponseBadRequest


class ExceptionHandler(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_exception(self, request, exception):
        if isinstance(exception, LoginNeeded):
            return HttpResponseRedirect(exception.login_url + '?next=' + request.path)
        if isinstance(exception, BadRequest):
            return HttpResponseBadRequest(exception.message)