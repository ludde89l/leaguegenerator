from django.contrib.auth.models import User
from rest_framework import viewsets
from competition import models
from football import serializers


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer


class TournamentViewSet(viewsets.ModelViewSet):
    queryset = models.Tournament.objects.all()
    serializer_class = serializers.TournamentSerializer
    lookup_field = 'ext_id'


class TieBreakerViewSet(viewsets.ModelViewSet):
    queryset = models.TieBreaker.objects.all()
    serializer_class = serializers.TieBreakerSerializer


class PlayoffViewSet(viewsets.ModelViewSet):
    queryset = models.Playoff.objects.all()
    serializer_class = serializers.PlayoffSerializer


class RoundViewSet(viewsets.ModelViewSet):
    queryset = models.Round.objects.all()
    serializer_class = serializers.GroupSerializer


class PlayoffRoundViewSet(viewsets.ModelViewSet):
    queryset = models.PlayoffRound.objects.all()
    serializer_class = serializers.PlayoffMatchSerializer


class PlayerViewSet(viewsets.ModelViewSet):
    queryset = models.Player.objects.all()
    serializer_class = serializers.PlayerSerializer
