from football.models import TieBreaker


class Match:

    def __init__(self, home_team, away_team, home_result, away_result, id):
        self.home_team = home_team
        self.away_team = away_team
        self.home_result = home_result
        self.away_result = away_result
        self.id = id

    @property
    def home_score(self):
        return self.home_result.score if self.home_result else None

    @property
    def away_score(self):
        return self.away_result.score if self.away_result else None


class Table:

    def __init__(self, group):
        self.group = group
        self.table = {}
        self.matches = []

    def add_game(self, player1, player2, id, player1_result=None, player2_result=None):
        player1_row = self.table.get(player1)
        if not player1_row:
            player1_row = Table.TableRow(self, player1)
            self.table[player1] = player1_row
        player2_row = self.table.get(player2)
        if not player2_row:
            player2_row = Table.TableRow(self, player2)
            self.table[player2] = player2_row
        if(player1_result and player2_result):
            player1_row.add_result(player1_result.score, player2_result.score)
            player2_row.add_result(player2_result.score, player1_result.score)
        self.matches.append(Match(player1, player2, player1_result, player2_result, id))

    def get_matches(self):
        return self.matches

    def get_table(self):
        return self.table.values()

    def lowest_promtion(self):
        return self.group.promotion_spots()

    def highest_relegation(self):
        return len(self.table) - self.group.relegation_spots() + 1

    class TableRow:
        def __init__(self, table, player):
            self.place = 0
            self.table = table
            self.player = player
            self.wins = 0
            self.draws = 0
            self.losses = 0
            self.goals_scored = 0
            self.goals_conceded = 0
            self.total_games = (table.group.no_competitors - 1) * table.group.rounds

        def add_result(self, player_score, opponent_result):
            if player_score > opponent_result:
                self.wins += 1
            elif player_score == opponent_result:
                self.draws += 1
            else:
                self.losses += 1
            self.goals_scored += player_score
            self.goals_conceded += opponent_result

        @property
        def points(self):
            return self.wins * 3 + self.draws

        @property
        def goal_difference(self):
            return self.goals_scored - self.goals_conceded

        @property
        def is_promoted(self):
            return self.place <= self.table.lowest_promtion()

        @property
        def is_relegated(self):
            return self.place >= self.table.highest_relegation()

        @staticmethod
        def can_pass(row, other_player):
            if other_player.place < row.place:
                return False
            max_points = (other_player.total_games - other_player.games_played) * 3 + other_player.points
            if max_points > row.points:
                return True
            if max_points == row.points and other_player.games_played != other_player.total_games:
                return True
            return False

        @property
        def secured_promotion(self):
            max_passage = self.table.lowest_promtion() - self.place
            if max_passage < 0:
                return False
            for row in self.table.table:
                if row is self:
                    continue
                if self.can_pass(self, row):
                    max_passage -= 1
                    if max_passage < 0:
                        return False
            return True

        @property
        def confirmed_relegated(self):
            needs_to_pass = self.place - self.table.highest_relegation() + 1
            if needs_to_pass < 0:
                return False
            for row in self.table.table:
                if row is self:
                    continue
                if self.can_pass(row, self):
                    needs_to_pass -= 1
                    if needs_to_pass < 0:
                        return False
            return True

        def __lt__(self, other):
            for tie_breaker in self.table.group.get_tournament().tie_breakers.all():
                tie_breaker = TieBreaker.objects.select_subclasses().get(type=tie_breaker.type)
                if tie_breaker.less_than(self, other):
                    return True
                if not tie_breaker.equals(self, other):
                    return False
            return False

        def __eq__(self, other):
            for tie_breaker in self.table.group.get_tournament().tie_breakers.all():
                tie_breaker = TieBreaker.objects.select_subclasses().get(type=tie_breaker.type)
                if not tie_breaker.equals(self, other):
                    return False
            return True