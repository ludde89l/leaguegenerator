from django.utils.translation import gettext as _

_("QUARTER")
_("SEMI")
_("FINAL")
_("Tournament won by %(player)s")
_("Tournament over")
_("Away goal")
_("Points")
_("Goal Difference")
_("Seed")
_("Scored goals")