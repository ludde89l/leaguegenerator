from django.contrib import admin
from football.models import GoalDifferenceTieBreaker, AwayGoalTieBreaker, PointTieBreaker, ScoredGoalsTieBreaker, SeedTieBreaker

admin.site.register(GoalDifferenceTieBreaker)
admin.site.register(AwayGoalTieBreaker)
admin.site.register(PointTieBreaker)
admin.site.register(ScoredGoalsTieBreaker)
admin.site.register(SeedTieBreaker)