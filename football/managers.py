from django.db import models


class GroupManager(models.Manager):
    def get_queryset(self):
        return super(GroupManager, self).get_queryset().filter(stage__type='GROUP')


class PlayoffRoundManager(models.Manager):
    def get_queryset(self):
        return super(PlayoffRoundManager, self).get_queryset().exclude(stage__type='GROUP')


class TournamentManager(models.Manager):
    def get_queryset(self):
        return super(TournamentManager, self).get_queryset().filter(type="FOOT")


class PlayoffManager(models.Manager):
    def get_queryset(self):
        return super(PlayoffManager, self).get_queryset().filter(tournament__type="FOOT")


class FootballMatchManager(models.Manager):
    def get_queryset(self):
        return super(FootballMatchManager, self).get_queryset().filter(type="FOOT")