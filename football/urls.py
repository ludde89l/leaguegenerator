"""football URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import football.views
from django.conf.urls import include, url
from rest_framework import routers
from football import view_sets

app_name = 'football'

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', view_sets.UserViewSet)
router.register(r'tournaments', view_sets.TournamentViewSet)
router.register(r'tie_breakers', view_sets.TieBreakerViewSet)
router.register(r'playoffs', view_sets.PlayoffViewSet)
router.register(r'rounds', view_sets.RoundViewSet)
router.register(r'playoffround', view_sets.PlayoffRoundViewSet)
router.register(r'players', view_sets.PlayerViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^addResult/(?P<match_id>[0-9]+)/$', football.views.add_result, name='add_result'),
    url(r'^addPlayoffResult/(?P<round_id>[0-9]+)/$', football.views.add_playoff_result, name='add_playoff_result'),
    url(r'^completeGroup/(?P<group_id>[0-9]+)/$', football.views.complete_group, name='complete_group'),
    url(r'^createLeague/$', football.views.AddLeague.as_view(), name='create_league'),
    url(r'^tournament/(?P<tournament_id>[A-Za-z0-9]+)/$', football.views.tournament, name='tournament'),
    url(r'^test/$', football.views.test, name='tournament'),
    url(r'^rest/', include(router.urls))
]
