from rest_framework import serializers
from competition import models
from football.models import GroupStage, PlayoffRound, FootballPlayoffRound
from django.db.models import Manager
from collections import OrderedDict
from django.utils.translation import gettext as _
from football import utils
from leaguegenerator.serializers import UserSerializer


class PlayerSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=False)
    class Meta:
        model = models.Player
        fields = ('display_name', 'user', 'id')


class PlayoffRoundListSerializer(serializers.ListSerializer):

    def to_representation(self, data):
        data = data.exclude(stage__type='GROUP').order_by('stage__order')
        return super(PlayoffRoundListSerializer, self).to_representation(data)


class GroupRoundListSerializer(serializers.ListSerializer):

    def to_representation(self, data):
        data = data.filter(stage__type='GROUP')
        return super(GroupRoundListSerializer, self).to_representation(data)


class TieBreakerNameSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        return _(instance)

class TieBreakerSerializer(serializers.ModelSerializer):

    name = TieBreakerNameSerializer()

    class Meta:
        model = models.TieBreaker
        fields = '__all__'


class PlayoffMatchSerializer(serializers.ModelSerializer):

    class Meta:
        model = PlayoffRound
        fields = ('id', 'name', 'competitors')


class PlayoffSerializer(serializers.Serializer):

    def get_tree(self, instance):
        final = instance.playoffround_set.get(stage__type='FINAL')
        current_round = [FootballPlayoffRound.objects.get(id=final.id)]
        tree = []
        while current_round:
            next_round = []
            for round in current_round:
                prev_rounds = round.playoffround_set.all()
                if prev_rounds:
                    competitors = round.competitors.all()
                    if competitors:
                        first_competitor = competitors.first()
                        prev_round = prev_rounds.get(competitors=first_competitor)
                        next_round.append(FootballPlayoffRound.objects.get(id=prev_round.id))
                        last_competitor = competitors.last()
                        if first_competitor != last_competitor:
                            prev_round = prev_rounds.get(competitors=last_competitor)
                            next_round.append(FootballPlayoffRound.objects.get(id=prev_round.id))
                        else:
                            #Only one team has progressed to the next round
                            prev_round = prev_rounds.exclude(id=prev_round.id).first()
                            next_round.append(FootballPlayoffRound.objects.get(id=prev_round.id))
                    else:
                        for playoff_round in prev_rounds.all():
                            next_round.append(FootballPlayoffRound.objects.get(id=playoff_round.id))
            tree.insert(0, {'matches': current_round, 'stage': current_round[0].stage})
            current_round = next_round
        return tree

    def to_representation(self, instance):
        ret = OrderedDict()
        ret['rounds'] = []
        for idx, round in enumerate(self.get_tree(instance)):
            serialized_round = {'matchups': [], 'stage': {'name':_(round['stage'].type), 'type': round['stage'].type}}
            for matchup in round['matches']:
                player1 = matchup.team1()
                if player1:
                    team1 = {'name':player1.get_display_name(), 'id': player1.id}
                else:
                    team1 = {'name':''}
                player2 = matchup.team2()
                if player2:
                    team2 = {'name':player2.get_display_name(), 'id': player2.id}
                else:
                    team2 = {'name':''}
                serialized_matchup = {'matches': [], 'team1': team1, 'team2': team2, 'id': matchup.id}
                for match in matchup.get_scores():
                    try:
                        serialized_matchup['matches'].append({'team1Score': match.team1_score, 'team2Score': match.team2_score})
                    except models.Result.DoesNotExist:
                        pass

                serialized_round['matchups'].append(serialized_matchup)

            ret['rounds'].append(serialized_round)

        return ret


class ResultSerializer(serializers.ModelSerializer):
    player = PlayerSerializer()

    class Meta:
        model = models.Result
        fields = ('score', 'player')


class MatchSerializer(serializers.ModelSerializer):
    competitors = PlayerSerializer(many=True)
    result_set = ResultSerializer(many=True)

    class Meta:
        model = models.Match
        fields = ('id', 'competitors','result_set')


class GroupSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        group = GroupStage.objects.get(id=instance.id)
        json_group = {'table': [], 'name': group.name, 'matches': [], "id": group.id}
        data = group.get_wins_draws_and_losses()
        for team in sorted(data.get_table(), key=utils.cmp_to_key(utils.tournament_compare, group.tournament), reverse=True):
            json_group['table'].append({'player': team.player.display_name,
                                        'wins': team.wins,
                                        'draws': team.draws,
                                        'losses': team.losses,
                                        'goals_scored': team.goals_scored,
                                        'goals_conceded': team.goals_conceded,
                                        'id': team.player.id})
        for match in data.get_matches():
            json_group['matches'].append({'home_team': {'name':match.home_team.display_name, 'id':match.home_team.id},
                                          'away_team': {'name':match.away_team.display_name, 'id':match.away_team.id},
                                          'home_result': match.home_score,
                                          'away_result': match.away_score,
                                          'id': match.id})
        return json_group

    class Meta:
        list_serializer_class = GroupRoundListSerializer
        model = GroupStage


class RuleListSerializer(serializers.ListSerializer):

    def to_representation(self, data):
        ret = {}
        iterable = data.all() if isinstance(data, Manager) else data
        for item in iterable:
            ret[item.key] = item.value
        return ret


class RuleSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Rule
        fields = '__all__'
        list_serializer_class = RuleListSerializer


class TournamentSerializer(serializers.ModelSerializer):
    tie_breakers = TieBreakerSerializer(many=True)
    round_set = GroupSerializer(many=True)
    rule_set = RuleSerializer(many=True)
    playoff = PlayoffSerializer()
    winners = PlayerSerializer(many=True)

    class Meta:
        model = models.Tournament
        fields = ('name','playoff','round_set', 'tie_breakers', 'rule_set', 'winners')
