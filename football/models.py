from django.db.models import Max
from django.shortcuts import resolve_url
from datetime import datetime

from competition.models import Round, Result, TieBreaker, Tournament, Match, MatchData, Playoff, PlayoffRound, MatchOrder, Rule, News, NewsData
from football.managers import *
from football.table_data import Table
from football.utils import FootballMatchResult

class FootballTournament(Tournament):
    objects = TournamentManager()

    class Meta:
        proxy = True

    def get_all_groups(self):
        groups = GroupStage.objects.filter(tournament=self)
        return groups

    def get_playoff(self):
        return FootballPlayoff.objects.get(tournament=self)


class FootballPlayoff(Playoff):
    objects = PlayoffManager()

    class Meta:
        proxy = True

    def add_competitor(self, player, group_no, placement):
        rounds = self.get_first_rounds()
        for round in rounds:
            if len(round.competitors.all()) < 2:
                round.competitors.add(player)
                return

    def get_first_rounds(self):
        order = self.playoffround_set.aggregate(Max('stage__order'))['stage__order__max']
        return FootballPlayoffRound.objects.filter(stage__order=order, playoff=self)


class FootballMatch(Match):
    objects = FootballMatchManager()

    class Meta:
        proxy = True

    @property
    def home_team(self):
        return MatchData.objects.get(placement='H', match=self).player

    @property
    def away_team(self):
        return MatchData.objects.get(placement='A', match=self).player

    def get_home_result(self):
        try:
            return Result.objects.get(match=self, player=self.home_team).score
        except Result.DoesNotExist:
            pass
        return None

    def get_away_result(self):
        try:
            return Result.objects.get(match=self, player=self.away_team).score
        except Result.DoesNotExist:
            pass
        return None

    def is_played(self):
        return not self.get_home_result() is None

    def __str__(self):
        if self.is_played():
            home_score = self.get_home_result()
            away_score = self.get_away_result()
            return str(self.home_team) + " " + str(home_score) + " - " + str(away_score) + " " + str(self.away_team)
        return str(self.home_team) + " - " + str(self.away_team)


class FootballPlayoffRound(PlayoffRound):
    objects = PlayoffRoundManager()

    class Meta:
        proxy = True

    def team1(self):
        player = self.competitors.first()
        return player

    def team2(self):
        team1 = self.competitors.first()
        player = self.competitors.last()
        if player == team1:
            return None
        return player

    def get_scores(self):
        return [FootballMatchResult(match) for match in self.matches.all()]

    def get_next_match_order(self):
        return (MatchOrder.objects.filter(round=self).aggregate(Max('order'))['order__max'] or 0) + 1

    def get_first_unplayed_match(self):
        for match in self.matches.all():
            if not Result.objects.filter(match=match).count():
                return match
        raise FootballMatch.DoesNotExist

    def player_proceeded(self, competitor):
        self.competitors.add(competitor)
        if len(self.competitors.all()) == 2:
            self.create_matches()

    def create_matches(self):
        if self.stage.type == 'FINAL':
            matches = Rule.objects.get(tournament=self.tournament, key='FINAL_ROUNDS').value
        else:
            matches = Rule.objects.get(tournament=self.tournament, key='PLAYOFF_ROUNDS').value
        min_matches = int(matches / 2) + 1
        for i in range(int(min_matches)):
            self.create_match()

    def player_won(self, competitor):
        if not self.competitors.filter(id=competitor.id).exists():
            raise ValueError(str(competitor) + " does not compete in this round!")
        if self.stage.type == 'FINAL':
            self.tournament.winners.add(competitor)
            news = News.objects.create(headline="Tournament over",
                                publish_date=datetime.now(),
                                text="Tournament won by %(player)s",
                                link=resolve_url('football:tournament', tournament_id=self.tournament.ext_id))
            NewsData.objects.create(news=news, key='player', value=competitor.display_name)
        else:
            FootballPlayoffRound.objects.get(id=self.next.id).player_proceeded(competitor)

    def is_over(self):
        if self.stage.type == 'FINAL':
            matches = int(Rule.objects.get(tournament=self.tournament, key='FINAL_ROUNDS').value)
        else:
            matches = int(Rule.objects.get(tournament=self.tournament, key='PLAYOFF_ROUNDS').value)
        player1_wins = 0
        player2_wins = 0
        player1 = self.competitors.first()
        player2 = self.competitors.last()
        for match in self.matches.all():
            try:
                player1_result = Result.objects.get(match=match, player=player1)
                player2_result = Result.objects.get(match=match, player=player2)
                if(player1_result.score > player2_result.score):
                    player1_wins += 1
                else:
                    player2_wins += 1
            except Result.DoesNotExist:
                pass
        return player1_wins * 2 > matches or player2_wins * 2 > matches

    def create_match(self):
        match = FootballMatch.objects.create(type='FOOT')
        MatchData.objects.create(match=match, player=self.competitors.first(), placement='N')
        MatchData.objects.create(match=match, player=self.competitors.last(), placement='N')
        MatchOrder.objects.create(match=match, round=self, order=self.get_next_match_order())


class GroupStage(Round):
    objects = GroupManager()

    class Meta:
        proxy = True

    def list_all_matches(self):
        matches = FootballMatch.objects.all().intersection(self.matches.all())
        return sorted(matches, key=lambda m: m.is_played(), reverse=True)

    def get_all_matches(self):
        return FootballMatch.objects.intersection(self.matches.all())

    def get_player_group_games(self, player):
        return self.matches.filter(competitors__id=player.id)

    def complete_group(self):
        table = sorted(self.get_wins_draws_and_losses().get_table(), key=lambda x: x.wins * 3 + x.draws, reverse=True)
        if not self.tournament.playoff:
            self.tournament.winners.append(table[0].player)
            return
        playoff = FootballPlayoff.objects.get(id=self.tournament.playoff.id)
        for i in range(int(self.tournament.rule_set.get(key="PROMOTED_TEAMS").value)):
            playoff.add_competitor(table[i].player)

    def generate_games(self):
        index = 0
        rounds = self.rounds
        competitors = self.competitors.all()
        if rounds % 2 == 1:
            rounds -= 1
            for index, player in enumerate(competitors):
                for player2 in competitors[index + 1:]:
                    match = FootballMatch.objects.create(type='FOOT')
                    MatchData.objects.create(match=match, player=player, placement='H')
                    MatchData.objects.create(match=match, player=player2, placement='A')
                    MatchOrder.objects.create(match=match, round=self, order=index)
        for _ in range(int(rounds / 2)):
            for player in self.competitors.all():
                for player2 in self.competitors.all():
                    if player == player2:
                        continue
                    match = FootballMatch.objects.create(type='FOOT')
                    MatchData.objects.create(match=match, player=player, placement='H')
                    MatchData.objects.create(match=match, player=player2, placement='A')
                    MatchOrder.objects.create(match=match, round=self, order=index)
                    index += 1
        return None

    def get_wins_draws_and_losses(self):
        table = Table(group=self)
        for match in self.matches.all():
            player1 = match.competitors.first()
            player2 = match.competitors.last()
            try:
                player1_result = Result.objects.get(match=match, player=player1)
                player2_result = Result.objects.get(match=match, player=player2)
                table.add_game(player1, player2, match.id, player1_result, player2_result)
            except Result.DoesNotExist:
                table.add_game(player1, player2, match.id)

        return table

    def get_tournament(self):
        return FootballTournament.objects.get(id=self.tournament.id)

    def promotion_spots(self):
        return int(Rule.objects.get(tournament=self.get_tournament(), key='PROMOTIONS').value)

    def relegation_spots(self):
        return int(Rule.objects.get(tournament=self.get_tournament(), key='RELEGATIONS').value)

    @property
    def no_competitors(self):
        return len(self.competitors.all())

    @property
    def rounds(self):
        return int(Rule.objects.get(tournament=self.get_tournament(), key='GROUP_ROUNDS').value)

class PointTieBreaker(TieBreaker):
    def compare(self, o1, o2):
        return o1.points - o2.points


class GoalDifferenceTieBreaker(TieBreaker):
    def compare(self, o1, o2):
        return o1.goal_difference - o2.goal_difference


class ScoredGoalsTieBreaker(TieBreaker):
    def compare(self, o1, o2):
        return o1.goals_scored - o2.goals_scored


class SeedTieBreaker(TieBreaker):
    def compare(self, o1, o2):
        return 0


class AwayGoalTieBreaker(TieBreaker):
    def compare(self, o1, o2):
        return 0
