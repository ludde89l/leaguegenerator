import ReactDOM from 'react-dom';
import React from 'react';
import Board from './board/board'
import {generateTree} from "./games/data";
import {observe, addTeam, setElements, getList} from './board/data'
import {Types} from './board/constants'

window.renderGroupTieBreakers = function(data, containers, callback = () => {}) {
    setElements(Types.GROUP_TIE_BREAKERS, data);
    for(let i = 0; i < containers.length; i++) {
        let container = document.getElementById(containers[i]);
        addObserver(Types.GROUP_TIE_BREAKERS, container, callback);
    }
};

window.renderPlayoffTieBreakers = function(data, container, callback = () => {}) {
    setElements(Types.PLAYOFF_TIE_BREAKERS, data);
    addObserver(Types.PLAYOFF_TIE_BREAKERS, container, callback);
};

function addObserver(type, container, callback) {
    observe(groups =>
        ReactDOM.render(
            <Board items={groups} callback={callback} type={type}/>,
            container
        ), type
    );
}

window.addElementToList = function(element) {
    addTeam(element);
};

window.generatePlayoff = function(stages) {
    generateTree(stages);
};

window.getGroups = function () {
    return getList(Types.GROUPS)
};

window.generateGroups = function(groups, container) {

    let rootEl = document.getElementById(container);
    let groupData = [];
    for(let i = 0; i < groups; i++) {
        let name = "Group " + String.fromCharCode(65 + i);
        groupData.push({ name: name, items: [] });
    }
    setElements(Types.GROUPS, groupData);
    addObserver(Types.GROUPS, rootEl, () => {});
};