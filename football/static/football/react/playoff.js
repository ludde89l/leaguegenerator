import React from 'react';
import ReactDOM from 'react-dom';
import {observe, setTree, addTeam, setGroups, getPlayoffMatches} from './games/data';
import Tree from './games/playoff/tree';
import {Types} from './games/constants';
import Schedule from "./games/group/schedule";

window.renderPlayoff = function(data) {
    setTree(data);
};

window.renderGroups = function(element, data) {
    let rootEl = document.getElementById(element);
    setGroups(data);
    observe(groups =>
        ReactDOM.render(
            <Schedule groups={groups.round_set}/>,
            rootEl
        ), Types.GROUPS
    );
};

window.createPlayoffTree = function(element, draggable = true, scoreable=false) {
    let rootEl = document.getElementById(element);
    observe(rounds => {
        ReactDOM.render(
            <Tree draggable={draggable} rounds={rounds} scorable={scoreable}/>,
            rootEl
        );}, Types.PLAYOFF);
};

window.addTeam = function(team) {
    addTeam(team);
};

window.getPlayoffMatches = function() {
    return getPlayoffMatches();
};