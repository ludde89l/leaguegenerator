import ReactDOM from 'react-dom';
import React from 'react';
import App from './board/board'

// ========================================
window.renderTest = function () {
    let items = {
        droppable1:{
            name:'Test0',
            items: [{id:'0-1', content:'0-1'}, {id:'0-2', content:'0-2'}]
        },
        droppable2:{
            name:'Test1',
            items: [{id:'1-1', content:'1-1'}, {id:'1-2', content:'1-2'}, {id:'1-3', content:'1-3'}]
        }
    };
    let rootEl = document.getElementById('test');
    ReactDOM.render(
        <App items={items}/>,
        rootEl
    )
};

