import React, { Component } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import List from './list'
import {moveElement} from "./data";

export default class Board extends Component {
    constructor(props) {
        super(props);
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        //List is 0 indexed but droppableId is 1 indexed
        let sourceList = result.source.droppableId - 1;
        let destList = result.destination.droppableId - 1;
        moveElement(result.source.index, result.destination.index, sourceList, destList, this.props.type);

        this.props.callback();
    }

    render() {
        let lists = this.props.items.map((item, index) => {
            //Droppable id can't be 0
            return <List name={item.name} items={item.items} droppableId={index + 1}>
            </List>
        });
        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                {lists}
            </DragDropContext>
        );
    }
}