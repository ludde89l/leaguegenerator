import React, { Component } from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";

function getListClasses(isDraggingOver) {
    return "drag-list rounded-bottom border-top-0 " + (isDraggingOver ? "bg-success" : "bg-primary");
}

function getItemClasses(isDragging) {
    let classes = "list-item font-weight-bold rounded border ";
    classes += isDragging ? "item-dragging" : "bg-light";
    return classes;
}

export default class List extends Component {
    render() {
        return (
            <div className="mx-2 col-3">
                <div className={"pt-2 border-bottom-0 rounded-top bg-primary text-light text-center font-weight-bold"}>{this.props.name}</div>
                <Droppable droppableId={this.props.droppableId}>
                    {(provided, snapshot) => (
                        <ul ref={provided.innerRef}
                            className={getListClasses(snapshot.isDraggingOver)}>
                            {this.props.items.map((item, index) => (
                                <Draggable key={item.id} draggableId={item.id} index={index}>
                                    {(provided, snapshot) => (
                                        <li ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={provided.draggableProps.style}
                                            className={getItemClasses(snapshot.isDragging)}
                                        >
                                            {item.content}
                                        </li>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </ul>
                )}
            </Droppable>
                    </div>
        );
    }
}