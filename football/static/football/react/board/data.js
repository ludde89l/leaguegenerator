import {Types} from './constants'

let objects = {};
let observer = {};

function emitChange(type) {
    if(observer[type]){
        let object = [];
        if(objects[type]) {
            object = objects[type];
        }
        for(let i = 0; i < observer[type].length; i++) {
            observer[type][i](object)
        }
    }
}

export function observe(o, type) {
    if (!observer[type]) {
        observer[type] = [];
    }

    observer[type].push(o);
    emitChange(type);
}

export function getList(type) {
    return objects[type];
}

export function moveElement(sourceIndex, destIndex, fromList, toList, type) {

    const [removed] = objects[type][fromList].items.splice(sourceIndex, 1);
    objects[type][toList].items.splice(destIndex, 0, removed);
    emitChange(type)
}

export function addTeam(team) {
    objects[Types.GROUPS][0].items.push(team);
    emitChange(Types.GROUPS);
}

export function setElements(type, element) {
    objects[type] = element;
    emitChange(type);
}