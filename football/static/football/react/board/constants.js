export const Types = {
    GROUP_TIE_BREAKERS: 'grouptiebreakers',
    PLAYOFF_TIE_BREAKERS: 'playofftiebreakers',
    GROUPS: 'groups'
};