export const ItemTypes = {
    TEAM: 'team',
};

export const Types = {
    PLAYOFF: 'playoff',
    GROUPS: 'groups',
    RULES: 'rules'
};