import React from 'react';
import Group from './group'

export default class Schedule extends React.Component {
    render() {
        const groups = this.props.groups.map((group) =>
            <Group key={group.id} group={group} />
        );
        return (<div>
            {groups}
        </div>);
    }
}