import React from 'react';
import $ from 'jquery'
import {addMatchResult} from "../data";

export default class Match extends React.Component {
    constructor(props) {
        super(props);
    }

    addScore() {
        if(this.props.match.home_result === null) {
            window.openDialog(this.props.match.home_team.name, this.props.match.away_team.name, 'group');
        }
    }

    static validateScore(score) {
        return score !== "" && score >= 0;
    }
    
    render() {

        let home_result;
        let away_result;
        let rowClass;
        if(this.props.match.home_result !== null) {
            home_result = this.props.match.home_result;
            away_result = this.props.match.away_result;
            rowClass = "";
        }
        else {
            home_result = "";
            away_result = "";
            rowClass = "matchRow";
        }
        return (
            <tr className={rowClass} onClick={this.addScore.bind(this)}>
                <td>{this.props.match.home_team.name}</td>
                <td className="groupScore">{home_result}</td>
                <td className="groupScore">-</td>
                <td className="groupScore">{away_result}</td>
                <td style={{'text-align':'right'}}>{this.props.match.away_team.name}</td>
            </tr>
        );
    }
}