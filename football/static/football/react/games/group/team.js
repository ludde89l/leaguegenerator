import React  from 'react';


export default class Team extends React.Component {

    render() {
        let wins = this.props.team.wins;
        let draws = this.props.team.draws;
        let losses = this.props.team.losses;
        return (<tr className={this.props.className}>
            <td>{ this.props.team.place }</td>
            <td>{ this.props.team.player }</td>
            <td>{ wins + draws + losses }</td>
            <td>{ wins }</td>
            <td>{ draws }</td>
            <td>{ losses }</td>
            <td>{ this.props.team.goals_scored }</td>
            <td>{ this.props.team.goals_conceded }</td>
            <td>{ this.props.team.goals_scored - this.props.team.goals_conceded }</td>
            <td>{ wins * 3 + draws }</td>
        </tr>);
    }
}