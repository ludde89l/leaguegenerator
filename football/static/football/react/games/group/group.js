import React from 'react';
import Team from './team';
import Match from './match';
import {matches, promotedTeams} from "../data";

export default class Group extends React.Component {

    securedPromotion(team) {
        let max_passage = promotedTeams() - team.place;
        if(max_passage < 0) {
            return false;
        }
        for(let i = 0; i < this.props.group.table.length; i++) {
            let row = this.props.group.table[i];
            if(row.player === team.player) {
                continue;
            }
            if(this.canPass(team, row)) {
                max_passage -= 1;
            }
            if(max_passage < 0) {
                return false;
            }
        }
        return true;
    }

    getClassName(team) {
        let ret = "";
        if(this.securedPromotion(team)) {
            ret += 'securedPromotion ';
        }
        if(team.place <= promotedTeams()) {
            ret += 'promotion';
        }
        return ret;
    }

    canPass(team, other_team) {
        if(other_team.place < team.place) {
            return false;
        }
        let wins = other_team.wins;
        let draws = other_team.draws;
        let losses = other_team.losses;
        let otherTeamPlayedGames = (wins + draws + losses);
        let max_points = ((matches() * this.props.group.table.length) - otherTeamPlayedGames) * 3 + other_team.points;
        if(max_points > team.points) {
            return true;
        }
        return max_points === team.points && otherTeamPlayedGames !== 6;

    }

    render() {
        if(!this.props.group) {
            return null;
        }
        let i = 1;
        const teams = this.props.group.table.map((team) =>
            {
                team.place = i++;
                return <Team key={team.id} team={team} className={this.getClassName(team)} />
            }
        );
        const matches = this.props.group.matches.map((match) =>
            <Match key={match.id} match={match} />
        );
        return ( <div className="group">
            { this.props.group.name }
            <table className="groupTable">
                <thead>
                    <tr>
                        <th>Place</th>
                        <th>Name</th>
                        <th>G</th>
                        <th>W</th>
                        <th>D</th>
                        <th>L</th>
                        <th>GF</th>
                        <th>GA</th>
                        <th>GD</th>
                        <th>P</th>
                    </tr>
                </thead>
                <tbody>
                    { teams }
                </tbody>
            </table>
            <table>
            <thead>
            </thead>
            <tbody>
            { matches }
            </tbody>
        </table>
        </div>);
    }
}