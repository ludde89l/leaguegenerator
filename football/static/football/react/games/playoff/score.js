import React, { Component } from 'react';

export default class Score extends Component {
    render() {
        let scores;
        if(this.props.deletable) {
            scores = <button type="button" className="btn btn-danger btn-sm" onClick={this.props.remove}>✘</button>
        }
        else {
            scores = this.props.scores.map((result) =>
                <span className="result">{result}</span>
            );
        }
        return (
            <span className="score">
                {scores}
            </span>
        );
    }
}