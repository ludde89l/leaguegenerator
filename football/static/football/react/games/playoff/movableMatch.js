import React from 'react';
import Match from './match'
import MovableMatchEntry from './movableMatchEntry'

export default class MovableMatch extends Match {


    render() {
        let matchEntry1 = <MovableMatchEntry key={"team1"} name={this.props.match.team1.name} scores={[]} placement="top" match={this.props.match_num}/>;
        let matchEntry2 = <MovableMatchEntry key={"team2"} name={this.props.match.team2.name} scores={[]} placement="bottom" match={this.props.match_num}/>;

        return (
            [matchEntry1,
                <li key={"game-spacer"} className="game game-spacer">&nbsp;</li>,
                matchEntry2,
                <li key={"spacer"} className="game spacer">&nbsp;</li>]
        );
    }
}