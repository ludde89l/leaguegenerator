import React  from 'react';
import PropTypes from 'prop-types';
import { ItemTypes } from '../constants';
import { DragSource } from 'react-dnd';
import Team from './team'

const teamSource = {
    beginDrag(props, monitor, component) {
        return {
            name:component.props.name,
            match:component.props.match,
            placement:component.props.placement
        };
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

class MovableTeam extends Team {

    cursor() {
        return 'move'
    }

    render() {
        const { connectDragSource } = this.props;
        return connectDragSource(super.render());
    }
}

MovableTeam.propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired
};

export default DragSource(ItemTypes.TEAM, teamSource, collect)(MovableTeam);