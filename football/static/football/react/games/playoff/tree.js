import React from 'react';
import Round from './round'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

class Tree extends React.Component {

    render() {
        const rounds = this.props.rounds.map((obj) =>
            <Round key={obj.stage} round={obj} draggable={obj === this.props.rounds[0] && this.props.draggable} scorable={this.props.scorable}/>
        );
        return (
            <div key={"tree"} className="playoffTree">
                { rounds }
            </div>
        );
    }
}
export default DragDropContext(HTML5Backend)(Tree);