import Team from "./team";
import React from 'react';


export default class ScorableTeam extends Team {

    cursor() {
        return this.props.cursor;
    }

    color() {
        return this.props.color;
    }

    mouseEnter() {
        this.props.onMouseOver();
    }

    mouseLeave() {
        this.props.onMouseLeave();
    }

    onClick() {
        this.props.onClick();
    }
}