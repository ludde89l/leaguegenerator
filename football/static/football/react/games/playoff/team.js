import React, { Component } from 'react';

export default class Team extends Component {

    cursor() {
        return 'default'
    }

    onClick() {}

    color() {
        return "black";
    }

    mouseEnter() {}

    mouseLeave() {}

    render() {
        return <span onClick={this.onClick.bind(this)}
                     onMouseEnter={this.mouseEnter.bind(this)}
                     onMouseLeave={this.mouseLeave.bind(this)}
                     style={{
                cursor: this.cursor(),
                float: 'left',
                color: this.color()
            }}>{ this.props.name }</span>;
    }
}