import React  from 'react';
import MovableTeam from './movableTeam'
import Score from './score'
import { ItemTypes } from '../constants';
import { DropTarget } from 'react-dnd';
import PropTypes from 'prop-types';
import {swapTeams, removeTeam} from "../data";
import MatchEntry from './matchEntry'

const squareTarget = {
    canDrop(props, monitor) {
        let item = monitor.getItem();
        return item.name !== props.name;
    },

    drop(props, monitor, component) {
        let dragItem = monitor.getItem();
        swapTeams(dragItem, component.props);
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    };
}

class MovableMatchEntry extends MatchEntry {

    removePlayer() {
        removeTeam(this.props);
    }

    backgroundColor() {
        const {isOver, canDrop } = this.props;
        let backgroundColor = canDrop ? "LemonChiffon" : "white";
        return isOver && canDrop ? "LightGreen" : backgroundColor;
    }

    renderTeam() {
        return <MovableTeam name={this.props.name} match={this.props.match} placement={this.props.placement}/>;
    }

    renderScore() {
        return <Score scores={[]} deletable={this.props.name} remove={this.removePlayer.bind(this)}/>
    }

    render() {
        const {connectDropTarget} = this.props;
        return connectDropTarget(super.render());
    }
}
MovableMatchEntry.propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired
};
export default DropTarget(ItemTypes.TEAM, squareTarget, collect)(MovableMatchEntry);
