import React from 'react';
import ScorableMatchEntry from './scorableMatchEntry'

export default class Match extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            textColor: 'black'
        };
    }

    render() {

        let team1Scores = this.props.match.matches.map((result) =>
            result.team1Score
        );
        let team2Scores = this.props.match.matches.map((result) =>
            result.team2Score
        );
        
        let hoverCursor = 'default';
        let matchEntry1 = <ScorableMatchEntry key={"team1"}
                                               textColor={this.state.textColor}
                                               name={this.props.match.team1.name}
                                               scores={team1Scores}
                                               placement="top"
                                               match={this.props.match_num}
                                               onMouseOver={() => {}}
                                               onMouseLeave={() => {}}
                                               onClick={() => {}}
                                               cursor={hoverCursor}/>;
        let matchEntry2 = <ScorableMatchEntry key={"team2"}
                                               textColor={this.state.textColor}
                                               name={this.props.match.team2.name}
                                               scores={team2Scores}
                                               placement="bottom"
                                               match={this.props.match_num}
                                               onMouseOver={() => {}}
                                               onMouseLeave={() => {}}
                                               onClick={() => {}}
                                               cursor={hoverCursor}/>;

        return (
            [matchEntry1,
                <li key={"game-spacer"} style={{'cursor': hoverCursor}} className="game game-spacer">&nbsp;</li>,
                matchEntry2,
                <li key={"spacer"} style={{'cursor': hoverCursor}} className="game spacer">&nbsp;</li>]
        );
    }
}