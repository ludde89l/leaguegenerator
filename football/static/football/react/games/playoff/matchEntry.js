import React, { Component } from 'react';
import Score from './score'
import Team from './team'

export default class MatchEntry extends Component {

    renderTeam(){
        return <Team name={this.props.name} match={this.props.match} placement={this.props.placement}/>;
    }

    backgroundColor() {
        return "white";
    }

    renderScore() {
        return <Score scores={this.props.scores} deletable={false} remove={() => {}}/>;
    }

    render() {
        return <li style={{ backgroundColor: this.backgroundColor() }} className={"game game-" + this.props.placement}>
            {this.renderTeam()}
            {this.renderScore()}
        </li>;
    }
}
