import MatchEntry from './matchEntry'
import ScorableTeam from './scorableTeam'
import React from 'react';


export default class ScorableMatchEntry extends MatchEntry {

    renderTeam(){
        return <ScorableTeam name={this.props.name}
                             color={this.props.textColor}
                             match={this.props.match}
                             placement={this.props.placement}
                             onClick={this.props.onClick}
                             onMouseOver={this.props.onMouseOver}
                             onMouseLeave={this.props.onMouseLeave}
                             cursor={this.props.cursor}/>;
    }
}
