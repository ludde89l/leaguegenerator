import React from 'react';
import Match from './match'
import {getName} from "../data";
import ScoreableMatch from './scoreableMatch'
import MovableMatch from './movableMatch'

export default class Round extends React.Component {

    createMatch(obj, i) {
        if(this.props.draggable) {
            return <MovableMatch key={obj.id} match_num={i} match={obj}/>
        }
        if(this.props.scorable) {
            return <ScoreableMatch key={obj.id} match_num={i} match={obj} stage={this.props.round.stage}/>
        }
        return <Match key={obj.id} match_num={i} match={obj}/>
    }

    render() {
        let i = 0;
        const matches = this.props.round.matchups.map((obj) =>
            this.createMatch(obj, i++)
        );

        return (
            <ul key={this.props.round.stage} className="round">
                <li className="spacer">&nbsp;</li>
                { matches }
            </ul>
        );
    }
}