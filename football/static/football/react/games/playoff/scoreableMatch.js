import React from 'react';
import Match from './match'
import ScorableMatchEntry from './scorableMatchEntry'
import {playoffRounds} from "../data";

export default class ScoreableMatch extends Match {

    isOver() {
        let matches = playoffRounds(this.props.stage.type);
        let playedMatches = this.props.match.matches.length;
        if(playedMatches === matches) {
            //All matches are played. It is over
            return true;
        }
        let targetWins = Math.floor(matches / 2) + 1;
        let player1Wins = 0;
        let player2Wins = 0;
        for (let i = 0; i < playedMatches; i++) {
            let match = this.props.match.matches[i];
            if(match.team1Score > match.team2Score) {
                player1Wins++;
            }
            else {
                player2Wins++;
            }
        }
        return player1Wins >= targetWins || player2Wins >= targetWins;

    }

    isScorable() {
        return this.props.match.team1.name && this.props.match.team2.name && !this.isOver()
    }

    addScore() {
        if(this.isScorable()) {
            window.openDialog(this.props.match.team1, this.props.match.team2, this.props.match.id, 'playoff');
        }
    }

    onMouseOver() {
        if(this.isScorable()) {
            this.setState({
                textColor: 'blue'
            })
        }
    }

    onMouseLeave() {
        this.setState({
            textColor: 'black'
        })
    }

    render() {
        let onMouseEnter = this.onMouseOver.bind(this);
        let onMouseLeave = this.onMouseLeave.bind(this);
        let clickFunction = this.addScore.bind(this);

        let hoverCursor = this.isScorable() ? 'pointer' : 'default';

        let team1Scores = [];
        let team2Scores = [];
        let playedMatches = this.props.match.matches.length;
        for (let i = 0; i < playedMatches; i++) {
            let match = this.props.match.matches[i];
            team1Scores.push(match.team1Score);
            team2Scores.push(match.team2Score);
        }

        let matchEntry1 = <ScorableMatchEntry key={"team1"}
                                           textColor={this.state.textColor}
                                           name={this.props.match.team1.name}
                                           scores={team1Scores}
                                           placement="top"
                                           match={this.props.match_num}
                                           onMouseOver={onMouseEnter}
                                           onMouseLeave={onMouseLeave}
                                           onClick={clickFunction}
                                           cursor={hoverCursor}/>;
        let matchEntry2 = <ScorableMatchEntry key={"team2"}
                                           textColor={this.state.textColor}
                                           name={this.props.match.team2.name}
                                           scores={team2Scores}
                                           placement="bottom"
                                           match={this.props.match_num}
                                           onMouseOver={onMouseEnter}
                                           onMouseLeave={onMouseLeave}
                                           onClick={clickFunction}
                                           cursor={hoverCursor}/>;

        return (
            [matchEntry1,
                <li key={"game-spacer"} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave} style={{'cursor': hoverCursor}} className="game game-spacer" onClick={clickFunction}>&nbsp;</li>,
                matchEntry2,
                <li key={"spacer"} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave} style={{'cursor': hoverCursor}} className="game spacer" onClick={clickFunction}>&nbsp;</li>]
        );
    }
}