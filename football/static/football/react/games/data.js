import {Types} from './constants'
import $ from "jquery";

let objects = {};
let observer = {};

function emitChange(type) {
    if(observer[type]){
        let object = [];
        if(objects[type]) {
            object = objects[type];
        }
        observer[type](object)
    }
}

export function observe(o, type) {
    if (observer[type]) {
        return;
    }

    observer[type] = o;
    emitChange(type);
}

export function promotedTeams() {
    return objects[Types.GROUPS].rule_set.PROMOTED_TEAMS;
}

export function matches() {
    return objects[Types.GROUPS].rule_set.GROUP_ROUNDS;
}

export function playoffRounds(type) {
    if(type === "FINAL") {
        return objects[Types.RULES].FINAL_ROUNDS;
    }
    return objects[Types.RULES].PLAYOFF_ROUNDS;
}

export function setTree(data) {
    objects[Types.PLAYOFF] = data.playoff.rounds;
    objects[Types.RULES] = data.rule_set;
    emitChange(Types.PLAYOFF);
}

export function setGroups(data) {
    objects[Types.GROUPS] = data;
    emitChange(Types.GROUPS);
}

export function generateTree(rounds) {
    let r = [];
    let tree = {'playoff':{'rounds' : r} };
    for(let i = 0; i < rounds; i++) {
        let num_matches = Math.pow(2, rounds - i - 1);
        r.push(generateRound(num_matches));
    }
    setTree(tree)
}

function generateRound(matches) {
    let ret = {};
    ret.stage = matches;
    ret.matchups = [];
    for(let i = 0; i < matches; i++) {
        let json_matchup = {'matches': [], 'id': i};
        json_matchup.team1 = {id:0, name:''};
        json_matchup.team2 = {id:0, name:''};
        ret.matchups.push(json_matchup);
    }
    return ret;
}

export function swapTeams(team1, team2) {
    let tree = objects[Types.PLAYOFF];
    let match = tree[0].matchups[team2.match];
    match['team' + (team2.placement === 'top' ? 1 : 2)].name = team1.name;
    match = tree[0].matchups[team1.match];
    match['team' + (team1.placement === 'top' ? 1 : 2)].name = team2.name;
    emitChange(Types.PLAYOFF);
}

export function removeTeam(team) {
    let tree = objects[Types.PLAYOFF];
    let match = tree[0].matchups[team.match];
    match['team' + (team.placement === 'top' ? 1 : 2)] = {'name':'', 'id': 0};
    emitChange(Types.PLAYOFF);
}

export function addTeam(team) {
    let tree = objects[Types.PLAYOFF];
    for(let i = 0; i < tree[0].matchups.length; i++) {
        let match = tree[0].matchups[i];
        if(!match.team1.name){
            match.team1.name = team;
            emitChange(Types.PLAYOFF);
            return;
        }
        if(!match.team2.name){
            match.team2.name = team;
            emitChange(Types.PLAYOFF);
            return;
        }
    }
}

export function getPlayoffMatches() {
    let tree = objects[Types.PLAYOFF];
    let correctRound = tree[0];
    return correctRound.matchups;
}

export function addMatchResult(matchId, player1Id, player2Id, player1Score, player2Score) {
    let json_data = {};
    json_data.player1 = player1Id;
    json_data.player2 = player2Id;
    json_data.player1Score = player1Score;
    json_data.player2Score = player2Score;

    $.ajax({
        type: 'POST',
        url: '/football/addResult/' + matchId + '/',
        data: JSON.stringify(json_data),
        beforeSend: handleCsrf
    }).done(function (data) {
        row.closest('.group').html(data);
    });
}