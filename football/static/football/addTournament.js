function createTournament() {
    let value = $('.active', "#generalTab").data( "value" );
    let data = {};
    data.name = $('#tournamentName').val();
    data.rules = {};
    data.rules['PLAYOFF_ROUNDS'] = $('#noRounds').val();
    data.rules['FINAL_ROUNDS'] = $('#noFinalRounds').val();
    data.rules['GROUP_ROUNDS'] = $('#noGroupRounds').val();
    data.visibility = $('input[name=visibility]:checked','#generalForm').val() === 'true';
    data.groups = [];
    let groups = getGroups();
    groups.forEach(function(reactGroup) {
        let group = {};
        data.groups.push(group);
        group.name = reactGroup.name;
        let teams = [];
        group.teams = teams;
        reactGroup.items.forEach(function(item) {
            teams.push(item.content);
        });
    });
    data.playoff = {};
    data.playoff.competitors = [];
    let matchups = getPlayoffMatches();
    matchups.forEach(function(matchup) {
        data.playoff.competitors.push(matchup.team1.name);
        data.playoff.competitors.push(matchup.team2.name);
    });
    $.ajax({
        type: 'POST',
        url: '/football/createLeague/',
        data: JSON.stringify(data),
        contentType: "application/json",
        beforeSend: handleCsrf
    }).done(function (data) {
        window.location.href = data.successUrl;
    });
}

function validate(tabId) {
    if(tabId === "generalPill") {
        let $tournamentName = $('#tournamentName');
        return validateInput($tournamentName, (input) => input.val())
    }
    else if(tabId === "groupPill") {
        let $numberOfGroups = $('#groupNo');
        let $numberOfPlayoffTeams = $('#playoffTeams');
        let valid = true;
        valid &= validateInput($numberOfGroups, (input) => input.val());
        valid &= validateInput($numberOfPlayoffTeams, (input) => input.val());
        return valid
    }
    else if(tabId === "playoffPill") {
        let valid = true;
        valid &= validateInput($( "#noPlayoffTeams" ), (input) => input.val());
        valid &= validateInput($( "#noRounds" ), (input) => input.val());
        valid &= validateInput($( "#noFinalRounds" ), (input) => input.val());
        return valid;
    }
    return true;
}

function validateInput($input, validation) {
    let valid = validation($input);
    if(!valid) {
        $input.addClass("is-invalid")
    }
    else {
        $input.removeClass("is-invalid")
    }
    return valid;
}

function getSeedGroups() {
    let seedGroups = $('#noGroups').val();
    if($('#random').is(':checked')) {
        seedGroups = 1;
    }
    return seedGroups;
}

function addLeaguePlayer() {
    let value = $('.active', "#generalTab").data( "value" );
    let $nameInput = $("#playerName");
    let name = $nameInput.val().trim();
    if(!name) {
        $nameInput.addClass("is-invalid");
    }
    if(value === "playoff") {
        addTeam(name);
    }
    else {
        let team = {'id': name, 'content': name};
        addElementToList(team)
    }
    $nameInput.val("");
}

document.getElementById("playerName").addEventListener("keydown", function(e) {
    if(e.keyCode===13) {
        addLeaguePlayer();
}}, false);

let $groupTab = $("#groupPill").parent();
let $playoffTab = $("#playoffPill").parent();
let $leagueTab = $("#leaguePill").parent();
let $groupList = $("#groups");
let $bracket = $("#bracket");

function leagueClicked() {
    hideTab($groupTab);
    hideTab($playoffTab);
    showTab($leagueTab);
    $groupList.removeClass("d-none");
    $bracket.addClass("d-none");
}
function playoffClicked() {
    hideTab($groupTab);
    showTab($playoffTab);
    hideTab($leagueTab);
    $groupList.addClass("d-none");
    $bracket.removeClass("d-none");
}
function playoffAndGroupClicked() {
    showTab($groupTab);
    showTab($playoffTab);
    hideTab($leagueTab);
    $groupList.addClass("d-none");
    $bracket.removeClass("d-none");
}

function switchTab(elem) {
    return validate(elem.id);
}