function addResult(link) {
    let row = $(link).closest('.results');
    let matchId = row.data("match-id");
    let player1Id = row.children( ".player1" ).first().data("player-id");
    let player2Id = row.children( ".player2" ).first().data("player-id");
    let player1Score = row.find( ".player1Score" ).first().val();
    let player2Score = row.find( ".player2Score" ).first().val();
    let score1Valid = validateScore(player1Score);
    let score2Valid = validateScore(player2Score);
    if(!score1Valid) {
        row.find( ".player1Score" ).first().attr('style', "border-radius: 5px; border:#FF0000 1px solid;");
    }
    if(!score2Valid) {
        row.find( ".player2Score" ).first().attr('style', "border-radius: 5px; border:#FF0000 1px solid;");
    }
    if(!score2Valid || !score1Valid) {
        return;
    }
    $.ajax({
        type: 'POST',
        url: '/football/addResult/' + matchId + '/',
        data: 'player1=' + player1Id + '&player2=' + player2Id + '&player1Score=' + player1Score + '&player2Score=' + player2Score,
        beforeSend: handleCsrf
    }).done(function (data) {
        row.closest('.group').html(data);
    });
}

function validateScore(score) {
    return score !== "" && score >= 0;
}