from competition.models import Result, MatchData, TieBreaker, TieBreakerOrder, Player
from django.contrib.auth.models import User


class FootballMatchResult:
    def __init__(self, match):
        self.match = match

    def get_home_team_score(self):
        try:
            competitor = MatchData.objects.get(match=self.match, placement='H')
            result = Result.objects.get(match=self.match, player=competitor).score
            return result
        except MatchData.DoesNotExist:
            return self.team1_score

    def get_away_team_score(self):
        try:
            competitor = MatchData.objects.get(match=self.match, placement='A')
            result = Result.objects.get(match=self.match, player=competitor).score
            return result
        except MatchData.DoesNotExist:
            return self.team2_score

    @property
    def team1_score(self):
        competitor = self.match.competitors.first()
        result = Result.objects.get(match=self.match, player=competitor).score
        return result

    @property
    def team2_score(self):
        competitor = self.match.competitors.last()
        result = Result.objects.get(match=self.match, player=competitor).score
        return result

def tournament_compare(player1, player2, tie_breakers):
    for tie_breaker in tie_breakers:
        cmp = tie_breaker.compare(player1, player2)
        if cmp:
            return cmp
    return 0

def cmp_to_key(mycmp, tournament):

    tie_breakers = [TieBreaker.objects.select_subclasses().get(type=x.tie_breaker.type)
                    for x in TieBreakerOrder.objects.filter(tournament=tournament).order_by('order')]

    class K:
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj, tie_breakers) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj, tie_breakers) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj, tie_breakers) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj, tie_breakers) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, other.obj, tie_breakers) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj, tie_breakers) != 0
    return K

def get_or_create_player(name):
    try:
        return Player.objects.get(display_name=name)
    except Player.DoesNotExist:
        try:
            user = User.objects.get(username=name)
            return Player.objects.get(user=user)
        except User.DoesNotExist:
            return Player.objects.create(display_name=name)
        except Player.DoesNotExist:
            return Player.objects.create(user=user)
