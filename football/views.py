import json

import math

from django.core.exceptions import PermissionDenied
from django.db import transaction, IntegrityError
from django.http import HttpResponseBadRequest, JsonResponse
from django.shortcuts import render, get_object_or_404, reverse, resolve_url
from django.views.generic import View


from competition.models import Player, Result, Stage, Rule, TournamentPermissions, PlayoffRound, Round, MatchData
from competition.exceptions import LoginNeeded, BadRequest
from football.models import FootballTournament, FootballMatch, GroupStage, FootballPlayoff, FootballPlayoffRound, MatchOrder
from football.utils import get_or_create_player


def check_permission(request, tournament, access):
    if not request.user.is_authenticated:
        raise LoginNeeded(reverse('competition:login'))
    try:
        permission = TournamentPermissions.objects.get(user=request.user, tournament=tournament)
    except TournamentPermissions.DoesNotExist:
        raise PermissionDenied()
    if access == TournamentPermissions.READ_WRITE and permission.access != access:
        raise PermissionDenied()


def tournament(request, tournament_id):
    context = {
        'tournament_id': tournament_id,
    }
    return render(request, 'football/league_and_playoff.html', context=context)


def test(request):
    context = {
        'name': 'Magnus"',
        'translation':'Hello ${name}'
    }
    return render(request, 'football/test.html', context=context)


class AddLeague(View):

    def _create_tree(self, playoff, next_round, no_rounds, competitors, match_num):
        if not no_rounds:
            name = competitors.pop()
            if not name:
                raise BadRequest("NOT_ENOUGH_PLAYERS")
            player = get_or_create_player(name)
            next_round.competitors.add(player)
            return
        if next_round:
            stage = Stage.objects.get(order=next_round.stage.order + 1)
            name = '{0} {1}'.format(stage.type, match_num)
        else:
            stage = Stage.objects.get(type="FINAL")
            name = stage.type
        round = PlayoffRound.objects.create(playoff=playoff, name=name, tournament=playoff.tournament, stage=stage, next=next_round)
        next_num = match_num * 2 - 1
        self._create_tree(playoff, round, no_rounds - 1, competitors, next_num)
        self._create_tree(playoff, round, no_rounds - 1, competitors, next_num + 1)

    def _create_rounds(self, competitors, playoff):
        rounds = math.log2(len(competitors))
        if not rounds.is_integer():
            raise BadRequest("INVALID_COMPETITOR_NUM")
        self._create_tree(playoff, None, rounds, competitors, 1)

    def get(self, request):
        return render(request, 'football/createLeague.html')

    @transaction.atomic
    def post(self, request):
        data = json.loads(request.body)
        try:
            tournament = FootballTournament.objects.create(name=data['name'], type=FootballTournament.FOOTBALL)
        except IntegrityError:
            return HttpResponseBadRequest("DUPLICATED_NAME")
        if(data['groups']):
            group_rounds = int(data['rules']['GROUP_ROUNDS'])
            Rule.objects.create(tournament=tournament, key='GROUP_ROUNDS', value=group_rounds)
        for group in data['groups']:
            competitors = []
            for competitor in group['teams']:
                competitor.append(get_or_create_player(competitor))
            football_group = GroupStage.objects.create(name=group.name, tournament=tournament, stage=Stage.objects.get(type='GROUP'), competitors=competitors)
            football_group.generate_games()

        if data['playoff']:
            matches = int(data['rules']['PLAYOFF_ROUNDS'])
            final_matches = int(data['rules']['FINAL_ROUNDS'])
            Rule.objects.create(tournament=tournament, key='PLAYOFF_ROUNDS', value=matches)
            Rule.objects.create(tournament=tournament, key='FINAL_ROUNDS', value=final_matches)
            playoff = FootballPlayoff.objects.create(tournament=tournament)
            self._create_rounds(data['playoff']['competitors'], playoff)
            for round in playoff.get_first_rounds():
                round.create_matches()
        return JsonResponse({'successUrl': resolve_url('football:tournament', tournament_id=tournament.ext_id)})


@transaction.atomic
def complete_group(request, group_id):
    group = get_object_or_404(GroupStage, id=group_id)
    group.complete_group()

@transaction.atomic
def add_playoff_result(request, round_id):
    round = FootballPlayoffRound.objects.get(id=round_id)
    return add_result(request, round.get_first_unplayed_match().id)

@transaction.atomic
def add_result(request, match_id):
    match = get_object_or_404(FootballMatch, id=match_id)
    data = json.loads(request.body)
    player1 = get_object_or_404(Player, id=data['player1'])
    player2 = get_object_or_404(Player, id=data['player2'])
    try:
        MatchData.objects.get(player=player1, match=match)
        MatchData.objects.get(player=player2, match=match)
    except MatchData.DoesNotExist:
        return HttpResponseBadRequest("Player 1 or Player 2 not in match")
    try:
        player1_score = int(data['player1Score'])
        player2_score = int(data['player2Score'])
    except ValueError:
        return HttpResponseBadRequest()
    Result.objects.create(player=player1, match=match, score=player1_score)
    Result.objects.create(player=player2, match=match, score=player2_score)
    round = Round.objects.get(matches__in=[match])
    if round.stage.type == "GROUP":
        context = {
            'group': GroupStage.objects.get(matches__in=[match])
        }
        return render(request, 'football/group.html', context=context)
    else:
        round = FootballPlayoffRound.objects.get(id=round.id)
        if round.is_over():
            winner = player1 if player1_score > player2_score else player2
            round.player_won(winner)
        else:
            try:
                round.get_first_unplayed_match()
            except FootballMatch.DoesNotExist:
                #No unplayed matches but round is not over. Create new match
                round.create_match()

        return JsonResponse({})
