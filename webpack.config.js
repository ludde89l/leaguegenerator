module.exports = {
  entry: [
    "./football/static/football/react/board/board.js",
    "./football/static/football/react/playoff.js",
    "./football/static/football/react/createLeague.js",
  ],
  output: {
    path: __dirname + '/football/static/football',
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015','react','stage-2']
        },
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
  ]
};

